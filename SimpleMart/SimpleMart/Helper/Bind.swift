//
//  Bind.swift
//  SimpleMart
//
//  Created by CI-Kurt on 2021/5/31.
//

class Bind<T> {
    typealias Listener = (T) -> Void
    var listener: Listener?
    
    var value:T  {
        didSet{
            listener?(self.value)
        }
    }
    
    init(value:T) {
        self.value = value
    }
    
    func bindUp(listener:Listener?){
        self.listener = listener
        self.listener?(value)
    }
    
    func renew(){
        listener?(self.value)
    }
}



