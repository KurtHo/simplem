//
//  Config.swift
//  SimpleMart
//
//  Created by CI-Kurt on 2021/5/31.
//

import Foundation

public func debug(_ items: String..., filename: String = #file, function : String = #function, line: Int = #line, separator: String = " ") {
    #if DEBUG
        let pretty = "\(URL(fileURLWithPath: filename).lastPathComponent) [#\(line)] \(function)\n\t-> "
        let output = items.map { "\($0)" }.joined(separator: separator)
        Swift.print(pretty+output)
    #else
        Swift.print("RELEASE MODE")
    #endif
}

public func debug(_ items: Any..., filename: String = #file, function : String = #function, line: Int = #line, separator: String = " ") {
    #if DEBUG
    let pretty = "\(URL(fileURLWithPath: filename).lastPathComponent) [#\(line)] \(function)\n\t-> "
        let output = items.map { "\($0)" }.joined(separator: separator)
        Swift.print(pretty+output)
    #else
        Swift.print("RELEASE MODE")
    #endif
}

class Configuration {
    #if RELEASE
    public static let mode = ConfigurationMode.release
    #elseif DEBUG
    public static let mode = ConfigurationMode.debug
    #else
    public static let mode = ConfigurationMode.other
    #endif
}

public enum ConfigurationMode {
    case release, debug, other
}
