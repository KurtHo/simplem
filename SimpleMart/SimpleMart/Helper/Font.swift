//
//  Font.swift
//  SimpleMart
//
//  Created by CI-Kurt on 2021/5/31.
//
import UIKit

class Font {
    
    class func setFont(fontName: FontName, fontSize: CGFloat) -> UIFont {
        
        var fontSize = fontSize
        
        switch UIScreen.main.bounds.height {
        case 568:
            if fontSize > 10 {
                fontSize -= 1
            }
        case 736:
            fontSize += 1
        default:
            break
        }
        
        return UIFont(name: fontName.rawValue, size: fontSize)!
    }
}

extension UILabel {
    
    /**
     - Parameters:
        - fontName: Select Font Size From FontName
        - size: Set Font Size
        - color: Set Font Color
     */
    
    func setFont(fontName: FontName, size: CGFloat, color: UIColor) {
        
        self.font = Font.setFont(fontName: fontName, fontSize: size)
        self.textColor = color
    }
}

extension UITextField {
    
    /**
     - Parameters:
     - fontName: Select Font Size From Font Name
     - size: Set Font Size
     - color: Set Font Color
     */
    
    func setFont(fontName: FontName, size: CGFloat, color: UIColor) {
        self.font = Font.setFont(fontName: fontName, fontSize: size)
        self.textColor = color
    }
    
    func setPlaceholder(color: UIColor) {
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: color])
    }
}

extension UITextView {
    
    /**
     - Parameters:
     - fontName: Select Font Size From Font Name
     - size: Set Font Size
     - color: Set Font Color
     */
    
    func setFont(fontName: FontName, size: CGFloat, color: UIColor) {
        self.font = Font.setFont(fontName: fontName, fontSize: size)
        self.textColor = color
    }
}

extension UIButton {
    
    /**
     - Parameters:
     - fontName: Select Font Size From Font Name
     - size: Set Font Size
     - color: Set Font Color
     */
    
    func setFont(fontName: FontName, size: CGFloat, color: UIColor) {
        self.titleLabel?.font = Font.setFont(fontName: fontName, fontSize: size)
        self.setTitleColor(color, for: .normal)
    }
}

enum FontName: String {
    case PingFangTCRegular = "PingFangTC-Regular"
    case PingFangTCMedium = "PingFangTC-Medium"
    case PingFangTCSemibold = "PingFangTC-Semibold"
    case SFProDisplayBold = "SFProDisplay-Bold"
    case SFProDisplayRegular = "SFProDisplay-Regular"
    case SFProDisplayMedium = "SFProDisplay-Medium"
    case SFProTextRegular = "SFProText-Regular"
    case SFProTextBold = "SFProText-Bold"
    case SFProTextMedium = "SFProText-Medium"
}


