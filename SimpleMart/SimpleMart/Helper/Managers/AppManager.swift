//
//  AppManager.swift
//  SimpleMart
//
//  Created by CI-Kurt on 2021/5/31.
//

import UIKit

protocol AppManagerDelegate: class {
    func orientationChanging()
}

class AppManager {
    static let shared = AppManager()
    weak var delegate:AppManagerDelegate?
    
    static var orientationChanging:Bool = false {
        didSet(newValue) {
            if newValue && AppManager.orientationChanging == false{
                AppManager.shared.delegate?.orientationChanging()
            }
            
        }
    }
    
    
    static func getNowVc() -> UIViewController? {
        let vc = getNowPresentedVc(with: getNowFlowRootVc())
        return getNowVc(with: vc)
    }


    static func getNowFlowRootVc() -> UIViewController? {
        return getNowVc(with: getRootVc())
    }

    static func getRootVc() -> UIViewController? {
        return (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController
    }
    
    
}

//MARK:- Private
extension AppManager {
    private static func getNowPresentedVc(with vc: UIViewController?) -> UIViewController? {
        guard let subVc = vc?.presentedViewController else {
            return vc
        }
        return getNowPresentedVc(with: subVc)
    }
    
    private static func checkShowVc(with vc: UIViewController?) -> UIViewController? {

        guard let vc = vc else {
            return nil
        }
        if let subVc = vc as? UITabBarController {

            if let topVc = getNowVc(with: subVc.children[subVc.selectedIndex]) {
                return checkShowVc(with: topVc)
            }
            return subVc.children[subVc.selectedIndex]
        }
        if let subVc = vc as? UINavigationController {

            if let topVc = getNowVc(with: subVc.children.last) {
                return checkShowVc(with: topVc)
            }
            return subVc.children.last
        }
        return vc
    }
    
    private static func getNowVc(with vc: UIViewController?) -> UIViewController? {

        guard let subVc = checkShowVc(with: vc) else {
            return vc
        }
        return subVc//getNowVc(with: subVc)
    }
}
