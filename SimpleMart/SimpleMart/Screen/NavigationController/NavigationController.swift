//
//  NavigationController.swift
//  SimpleMart
//
//  Created by CI-Kurt on 2021/5/31.
//

import UIKit

class NavigationController:UINavigationController {
    
    let views = NavigationControllerViews()
    
    override func viewDidLoad() {
        views.setViews(superView: view, navigationBar: navigationBar)    
    }
    
    
    func showTitleLabel(text:String?){
        if let text = text {
            if text == views.titleLabel.text {return}
        }
        views.titleLabel.alpha = 0
        
        UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.2, delay: .zero, options: .curveLinear) {
            self.views.logoIv.alpha = 0
            self.views.titleLabel.alpha = 1
            self.views.titleLabel.text = text
        }

    }
    
    func showLogo(){
        if self.views.logoIv.alpha != 0 {
            return
        }
        self.views.titleLabel.text = ""
        UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.2, delay: .zero, options: .curveLinear) {
            self.views.logoIv.alpha = 1
            self.views.titleLabel.alpha = 0
        }
    }
}
