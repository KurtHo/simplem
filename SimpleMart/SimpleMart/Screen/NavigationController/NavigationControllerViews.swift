//
//  NavigationControllerViews.swift
//  SimpleMart
//
//  Created by CI-Kurt on 2021/5/31.
//

import UIKit

class NavigationControllerViews {
    
    var view: UIView!
    var navigationBar: UINavigationBar!
    
    let naviCenterView = UIView()
    
    var logoIv:UIImageView = {
        let logoImage = UIImage(imageLiteralResourceName: "naviLogo")
        let logoIv = UIImageView(image:logoImage)
        return  logoIv
    }()
    
    var titleLabel:UILabel = {
        let lb = UILabel()
        lb.setFont(fontName: .PingFangTCRegular, size: 16, color: .naviTitle)
        lb.textAlignment = .center
        return lb
    }()
    
    var searchBtn:UIButton = {
        let searchImg = UIImage(imageLiteralResourceName: "search")
        let searchBtn = UIButton()
        searchBtn.setImage(searchImg, for: .normal)
        return searchBtn
    }()
    var mailBtn:UIButton = {
        let mailImg = UIImage(imageLiteralResourceName: "mail")
        let mailBtn = UIButton()
        mailBtn.setImage(mailImg, for: .normal)
        return mailBtn
    }()
    var questionBtn:UIButton = {
        let questionImg = UIImage(imageLiteralResourceName: "question")
        let questionBtn = UIButton()
        questionBtn.setImage(questionImg, for: .normal)
        return questionBtn
    }()
    
    func setViews(superView:UIView, navigationBar:UINavigationBar) {
        self.view = superView
        self.navigationBar = navigationBar
        
        view.addSubview(naviCenterView)
        naviCenterView.setAnchor(top: navigationBar.topAnchor, leading: nil, trailing: nil, bottom: navigationBar.bottomAnchor, height: nil, width: 125, padding: UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0))
        naviCenterView.setCenterAnchor(centerX: view.centerXAnchor, centerY: nil)
        view.layoutSubviews()
        
        naviCenterView.addSubview(logoIv)
        logoIv.setAnchor(top: naviCenterView.topAnchor, leading: naviCenterView.leadingAnchor, trailing: naviCenterView.trailingAnchor, bottom: naviCenterView.bottomAnchor, height: nil, width: nil)

        naviCenterView.addSubview(titleLabel)
        titleLabel.setAnchor(top: navigationBar.topAnchor, leading: nil, trailing: nil, bottom: navigationBar.bottomAnchor, height: nil, width: 125, padding: UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0))
        titleLabel.setCenterAnchor(centerX: view.centerXAnchor, centerY: nil)
        titleLabel.alpha = 0
                
        view.addSubview(searchBtn)
        searchBtn.setAnchor(top: naviCenterView.topAnchor, leading: view.leadingAnchor, trailing: nil, bottom: naviCenterView.bottomAnchor, height: nil, width: naviCenterView.frame.height + 16, padding: UIEdgeInsets(top: -8, left: 8, bottom: 8, right: 0))

        view.addSubview(mailBtn)
        mailBtn.setAnchor(top: naviCenterView.topAnchor, leading: nil, trailing: view.trailingAnchor, bottom: naviCenterView.bottomAnchor, height: nil, width: naviCenterView.frame.height + 16, padding: UIEdgeInsets(top: -8, left: 0, bottom: 8, right: -2))

        view.addSubview(questionBtn)
        questionBtn.setAnchor(top: naviCenterView.topAnchor, leading: nil, trailing: mailBtn.leadingAnchor, bottom: naviCenterView.bottomAnchor, height: nil, width: naviCenterView.frame.height + 16, padding: UIEdgeInsets(top: -8, left: 0, bottom: 8, right: 6))
    }
    
    
}
