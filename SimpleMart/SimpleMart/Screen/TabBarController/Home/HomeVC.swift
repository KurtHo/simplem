//
//  HomeVC.swift
//  SimpleMart
//
//  Created by CI-Kurt on 2021/5/31.
//

import UIKit

class HomeVC:BaseVC {
    let vm = HomeVM()
    
    var tableView = UITableView()
    var headerView: HomeTableViewHeaderView!
    
    override func viewDidLoad() {
        setTableView()
    }
    
    private func setTableView(){
        view.addSubview(tableView)
        tableView.setAnchor(top: view.topAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: view.bottomAnchor, height: nil, width: nil)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        view.layoutSubviews()
        setHeader()
    }
    
    private func setHeader(){
        let ratio = self.tableView.bounds.size.width / 232
        let height =  self.tableView.bounds.size.width / ratio
        
        headerView = HomeTableViewHeaderView(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: height))
        
        tableView.tableHeaderView = headerView
        headerView.setContent(vm: vm)
    }
    
    //MARK:- Landscape
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        headerView.transition()
    }
}

//MARK:- UITableViewDataSource
extension HomeVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 :7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = "test"
        
        if indexPath.section == 0 && indexPath.row == 0 {
            let cell = HomeOperateViewCell()
            return cell
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
}

//MARK:- UITableViewDelegate
extension HomeVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch (indexPath.section, indexPath.row) {
        case (0,0):
            return 80
        default:
            return 100
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
  

}

