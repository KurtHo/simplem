//
//  HomeOperateViewCell.swift
//  SimpleMart
//
//  Created by CI-Kurt on 2021/6/10.
//

import UIKit

class HomeOperateViewCell: BaseTableViewCell {
    
    var items:[OperationItem] = [
        OperationItem(title: "搜尋許願品", image: UIImage(imageLiteralResourceName: "searchOrange")),
        OperationItem(title: "交易紀錄", image: UIImage(imageLiteralResourceName: "tradeRecord")),
        OperationItem(title: "許願單", image: UIImage(imageLiteralResourceName: "wishList")),
        OperationItem(title: "優惠券", image: UIImage(imageLiteralResourceName: "coupon")),
    ]
    var buttons:[UIButton] = []
    private let view:UIView = {
        let view = UIView()
        view.layer.cornerRadius = 12
        view.clipsToBounds = true
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.operatorSepLine.cgColor
        return view
    }()
    
    
    override init() {
        super.init()
        setContent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func setContent(){
        addSubview(view)
        view.setAnchor(top: topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: bottomAnchor, height: nil, width: nil, padding: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: -20))
        
        items.enumerated().forEach {
            let button = UIButton()

            button.setImage($1.image, for: .normal)
            button.setTitle($1.title, for: .normal)
            button.titleLabel?.textAlignment = .center
            button.setFont(fontName: .PingFangTCRegular, size: 12, color: .themeOrnage)
            
            self.view.addSubview(button)
            self.buttons.append(button)
        }
        
        adjustButtonsPosition()
        
        
        var leading: NSLayoutXAxisAnchor = view.leadingAnchor
        var trailing: NSLayoutXAxisAnchor?
        buttons.enumerated().forEach { index, btn in
            if btn == buttons.last {
                trailing = view.trailingAnchor
            }
            if btn != buttons.first {
                leading = buttons[index - 1].trailingAnchor
            }
            
            btn.setAnchor(top: view.topAnchor, leading: leading, trailing: trailing, bottom: view.bottomAnchor, height: nil, width: nil)
            
            let widthBtn:UIButton = index == 0 ? self.buttons[1] : self.buttons[index - 1]
            
            btn.widthAnchor.constraint(equalTo: widthBtn.widthAnchor).isActive = true
            
        }
        
        setSeperator()
    }
    
    private func adjustButtonsPosition(){
        let padding:CGFloat = 4
        buttons.forEach {
            $0.layoutSubviews()
            if let image = $0.imageView?.image, let title = $0.titleLabel?.attributedText {
                let imageSize = image.size
                let titleSize = title.size()
                
                $0.titleEdgeInsets.left = -imageSize.width
                $0.imageEdgeInsets.right = -titleSize.width

                if $0 == buttons[0] {
                    $0.titleEdgeInsets.bottom = -(imageSize.height + padding*2)
                    $0.imageEdgeInsets.top = -titleSize.height - padding/2

                } else {
                    $0.titleEdgeInsets.bottom = -(imageSize.height + padding)
                    $0.imageEdgeInsets.top = -(titleSize.height + padding)

                }
            }

        }
    }
    
    private func setSeperator(){
        layoutSubviews()
        view.layoutSubviews()
        let midView = UIView()
        let leftView = UIView()
        let rightView = UIView()
        [leftView, midView, rightView].forEach {
            self.view.addSubview($0)
            $0.backgroundColor = .operatorSepLine
        }
        
        let unifyPadding = UIEdgeInsets(top: 16, left: 0, bottom: -16, right: 0)
        let width:CGFloat = 1
        
        
        midView.setCenterAnchor(centerX: view.centerXAnchor, centerY: view.centerYAnchor)
        
        midView.setAnchor(top: view.topAnchor, leading: nil, trailing: nil, bottom: view.bottomAnchor, height: nil, width: width, padding: unifyPadding)
        
        
        leftView.setAnchor(top: view.topAnchor, leading: self.buttons.first?.trailingAnchor, trailing: nil, bottom: view.bottomAnchor, height: nil, width: width, padding: unifyPadding)
        
        
        rightView.setAnchor(top: view.topAnchor, leading: nil, trailing: self.buttons.last?.leadingAnchor, bottom: view.bottomAnchor, height: nil, width: width, padding: unifyPadding)
        
    }
    
}

class OperationItem {
    var title:String
    var image:UIImage
    init(title:String, image:UIImage) {
        self.title = title
        self.image = image
    }
}
