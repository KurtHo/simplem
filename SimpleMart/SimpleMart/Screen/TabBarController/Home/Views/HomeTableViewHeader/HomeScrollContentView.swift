//
//  HomeScrollContentView.swift
//  SimpleMart
//
//  Created by CI-Kurt on 2021/6/11.
//

import UIKit

class HomeScrollContentView: UIView {
    var label: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.setFont(fontName: .PingFangTCSemibold, size: 20, color: .black)
        return label
    }()
    private let text:String
    
    init(text:String){
        self.text = text
        label.text = text
        super.init(frame: .zero)
        
        addSubview(label)
        setPosition()
    }
    
    func setPosition(){
        self.label.setAnchor(top: nil, leading: nil, trailing: nil, bottom: nil, height: 80, width: 160)
        self.label.setCenterAnchor(centerX: self.centerXAnchor, centerY: self.centerYAnchor)
        
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
