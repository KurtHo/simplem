//
//  TabBarController.swift
//  SimpleMart
//
//  Created by CI-Kurt on 2021/5/31.
//

import UIKit

class TabBarController: UITabBarController {
    
    let homeVC = HomeVC()
    let wishVC = WishVC()
    let shelfVC = BaseVC()
    let cartVC = BaseVC()
    let memberVC = BaseVC()
    
    override func viewDidLoad() {
        setVCs()
        setTabBarStytle()
        delegate = self
    }
    
    private func setVCs() {
        homeVC.tabBarItem.image = UIImage(imageLiteralResourceName: "home")
        homeVC.tabBarItem.title = "首頁"
        
        wishVC.tabBarItem.image = UIImage(imageLiteralResourceName: "catalog")
        wishVC.tabBarItem.title = "許願分類"
        
        shelfVC.tabBarItem.image = UIImage(imageLiteralResourceName: "shelf")
        shelfVC.tabBarItem.title = "便利架"
        
        cartVC.tabBarItem.image = UIImage(imageLiteralResourceName: "cart")
        cartVC.tabBarItem.title = "購物車"
        cartVC.tabBarItem.badgeColor = .themeOrnage
        
        memberVC.tabBarItem.image = UIImage(imageLiteralResourceName: "member")
        memberVC.tabBarItem.title = "會員中心"
        
        viewControllers = [homeVC, wishVC, shelfVC, cartVC, memberVC]
    }
    
    private func setTabBarStytle(){
        tabBar.tintColor = .tabSelected
        tabBar.unselectedItemTintColor = .tabUnselected
    }
    
    private func test(){
        viewControllers?[3].tabBarItem.badgeValue = "2"
    }
}

extension TabBarController:UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let navi = viewController.navigationController as? NavigationController
        if viewControllers?.isEmpty == false {
            switch viewController {
            case homeVC:
                navi?.showLogo()
                
            case wishVC:
                self.test()
                navi?.showTitleLabel(text: viewController.tabBarItem.title)
                
            case shelfVC:
                navi?.showTitleLabel(text: viewController.tabBarItem.title)
                
            case cartVC:
                viewController.tabBarItem.badgeValue = nil
                navi?.showTitleLabel(text: viewController.tabBarItem.title)
                
            default:
                navi?.showTitleLabel(text: viewController.tabBarItem.title)
            }
            
  
        }
    }
}
