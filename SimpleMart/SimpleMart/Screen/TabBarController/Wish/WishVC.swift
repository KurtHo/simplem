import UIKit

class WishVC: UIViewController {
    
    let list = [ #colorLiteral(red: 0.5843137255, green: 0.8823529412, blue: 0.8274509804, alpha: 1) ,#colorLiteral(red: 0.9529411765, green: 0.5058823529, blue: 0.5058823529, alpha: 1), #colorLiteral(red: 0.9882352941, green: 0.8901960784, blue: 0.5411764706, alpha: 1), #colorLiteral(red: 0.5843137255, green: 0.8823529412, blue: 0.8274509804, alpha: 1), #colorLiteral(red: 0.9529411765, green: 0.5058823529, blue: 0.5058823529, alpha: 1)]
    var collectionView: UICollectionView!
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: view.frame.size.height)
        flowlayout.minimumInteritemSpacing = 0
        flowlayout.minimumLineSpacing = 0


        flowlayout.scrollDirection = .horizontal
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        
        view.addSubview(collectionView)
        collectionView.setAnchor(top: view.topAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: view.bottomAnchor, height: nil, width: nil)
        collectionView.backgroundColor = .white
        
        setCollectionView()
    }
  
    private func setCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "MyCell")
//        self.collectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .left, animated: false)
        
        automaticallyAdjustsScrollViewInsets = false
    }
    
    
    override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
        self.collectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .left, animated: false)
    }

}

extension WishVC: UICollectionViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageFloat = (scrollView.contentOffset.x / scrollView.frame.size.width)
        let pageInt = Int(round(pageFloat))
        
        switch pageInt {
        case 0:
            collectionView.scrollToItem(at: [0, 3], at: .left, animated: false)
        case list.count - 1:
            
            collectionView.scrollToItem(at: [0, 1], at: .left, animated: false)
        default:
            break
        }
    }
}


extension WishVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath)
        cell.contentView.backgroundColor = list[indexPath.row]
        return cell
    }
}

extension WishVC: UICollectionViewDelegateFlowLayout {

}
