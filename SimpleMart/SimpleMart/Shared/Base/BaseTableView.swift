//
//  BaseTableView.swift
//  SimpleMart
//
//  Created by CI-Kurt on 2021/5/31.
//
import UIKit

class BaseTableView: UITableView {
    
    init() {
        super.init(frame: .zero, style: .plain)
//        separatorStyle = .none
        separatorColor = .clear
        backgroundColor = .lightGray
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

