//
//  BaseTableViewCell.swift
//  SimpleMart
//
//  Created by CI-Kurt on 2021/5/31.
//

import UIKit

class BaseTableViewCell: UITableViewCell {
    
    init() {
        super.init(style: .default, reuseIdentifier: nil)
        selectionStyle = .none
        separatorInset = .zero
//        backgroundColor = .clear
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
