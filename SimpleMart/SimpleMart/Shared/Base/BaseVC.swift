//
//  BaseVC.swift
//  SimpleMart
//
//  Created by CI-Kurt on 2021/5/31.
//

import UIKit

class BaseVC:UIViewController {
    
    override func viewDidLoad() {
        view.backgroundColor = .white
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        AppManager.orientationChanging = true
    }
    
    override func viewDidLayoutSubviews() {
        AppManager.orientationChanging = false
    }
}
